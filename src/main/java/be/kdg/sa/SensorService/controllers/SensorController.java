package be.kdg.sa.SensorService.controllers;

import be.kdg.sa.SensorService.model.SensorMessage;
import be.kdg.sa.SensorService.model.SensorType;
import be.kdg.sa.SensorService.persistence.SensorMessageService;
import lombok.AllArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/sensormessages")
@AllArgsConstructor
public class SensorController {
    private final SensorMessageService sensorMessageService;

    @GetMapping("/filter/{sensortype}/{start}/{end}")
    public List<SensorMessage> getFilteredSensormessages(@PathVariable(value = "sensortype") SensorType sensorType,
                                                         @PathVariable(value = "start") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDateTime start,
                                                         @PathVariable(value = "end")
                                                         @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDateTime end) {
        return sensorMessageService.filterByTypeAndTimestamp(sensorType, start, end);
    }

    @GetMapping("/sensortype/{sensortype}")
    public List<SensorMessage> getFilteredSensormessages(@PathVariable(value = "sensortype") SensorType sensorType) {
        return sensorMessageService.getSensormessagesBySensorType(sensorType);
    }

    @GetMapping("/linechart")
    public ModelAndView getLineChartValues() {
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("linechart");
        List<Double> messages = sensorMessageService.getLineChartvalues();
        modelAndView.getModel().put("datapoints", messages);
        return modelAndView;
    }

    @GetMapping("heatmap")
    public ModelAndView getHeatMapValues() {
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("heatmap");
        List<Double> yCoords = sensorMessageService.getYcoords();
        List<Double> xCoords = sensorMessageService.getXcoords();
        modelAndView.getModel().put("yCoords", yCoords);
        modelAndView.getModel().put("xCoords", xCoords);
        return modelAndView;
    }

}
