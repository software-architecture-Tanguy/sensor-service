package be.kdg.sa.SensorService.controllers;

import be.kdg.sa.SensorService.model.SensorMessage;
import be.kdg.sa.SensorService.persistence.SensorMessageService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@RestController
@RequestMapping("/")
@AllArgsConstructor
public class WebController {

    private final SensorMessageService sensorMessageService;

    @GetMapping("/index")
    public ModelAndView getIndex() {
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("index");
        List<SensorMessage> sensorMessages = sensorMessageService.loadAllSensorMessages();
        modelAndView.getModel().put("sensormessages",sensorMessages);
        return modelAndView;
    }

    @GetMapping("getAllSensormessages")
    public List<SensorMessage> getallSensormessages() {
        return sensorMessageService.loadAllSensorMessages();
    }
}
