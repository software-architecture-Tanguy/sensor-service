package be.kdg.sa.SensorService.processing;

import be.kdg.sa.SensorService.model.SensorMessage;
import be.kdg.sa.SensorService.persistence.SensorMessageRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class Processor {
    private SensorMessageRepository sensorMessageRepository;

    public Processor (SensorMessageRepository sensorMessageRepository) {
        this.sensorMessageRepository = sensorMessageRepository;
    }

    public void processNewMessage(SensorMessage sensorMessage) {
        SensorMessage message = sensorMessageRepository.save(sensorMessage);
        log.info("Message was saved to the database!" + message.toString());
    }

}
