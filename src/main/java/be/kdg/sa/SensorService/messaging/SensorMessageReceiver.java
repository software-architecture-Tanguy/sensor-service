package be.kdg.sa.SensorService.messaging;

import be.kdg.sa.SensorService.mapping.JSONService;
import be.kdg.sa.SensorService.model.SensorMessage;
import be.kdg.sa.SensorService.processing.Processor;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Component;


@Component
@RabbitListener(queues = "SAMessages")
@AllArgsConstructor
@Slf4j
public class SensorMessageReceiver {
    private final Processor processor;
    private final RetryTemplate retryTemplate;
    private final JSONService jsonService;

    @RabbitHandler
    public void receiveMessage(String message) {
        retryTemplate.execute(arg0 -> {
            SensorMessage sensorMessage = (SensorMessage) jsonService.convertJSONToObject(message, SensorMessage.class);
            if (sensorMessage == null) {
                try {
                    Thread.sleep(1000);
                    log.info("No messages found, idling");
                } catch (InterruptedException e) {
                    log.warn("The queue has been interrupted.");
                }
            } else {
                try {
                    processor.processNewMessage(sensorMessage);
                } catch (Exception e) {
                    log.warn("Unable to process message!");
                }
            }
            return null;
        });
    }

}
