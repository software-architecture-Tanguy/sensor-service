package be.kdg.sa.SensorService.persistence;

import be.kdg.sa.SensorService.model.SensorMessage;
import be.kdg.sa.SensorService.model.SensorType;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class SensorMessageService {
    private final SensorMessageRepository sensorMessageRepository;

    public List<SensorMessage> loadAllSensorMessages() {
        return sensorMessageRepository.findAll();
    }

    public List<SensorMessage> filterByTypeAndTimestamp(SensorType sensorType, LocalDateTime start, LocalDateTime end) {
        return sensorMessageRepository.findAllBySensorTypeAndTimestampIsBetween(sensorType, start, end);
    }

    public List<SensorMessage> getSensormessagesBySensorType(SensorType sensorType) {
        return sensorMessageRepository.findAllBySensorType(sensorType);
    }

    public List<Double> getLineChartvalues() {
        List<SensorMessage> list = loadAllSensorMessages();
        List<Double> chartvalues = new ArrayList<>();
        for (SensorMessage sensorMessage : list) {
            if ((Double) sensorMessage.getValue() != null) {
                chartvalues.add(sensorMessage.getValue());
            }
        }
        return chartvalues;
    }


    public List<Double> getYcoords() {
        List<SensorMessage> messages = loadAllSensorMessages();
        List<Double> yCoords = new ArrayList<>();
        for (SensorMessage sensorMessage : messages) {
            if ((Double) sensorMessage.getYCoord() != null) {
                yCoords.add(sensorMessage.getYCoord());
            }
        }
        return yCoords;
    }

    public List<Double> getXcoords() {
        List<SensorMessage> messages = loadAllSensorMessages();
        List<Double> xCoords = new ArrayList<>();
        for (SensorMessage sensorMessage : messages) {
            if ((Double) sensorMessage.getXCoord() != null) {
                xCoords.add(sensorMessage.getYCoord());
            }
        }
        return xCoords;
    }
}

