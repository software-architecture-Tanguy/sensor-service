package be.kdg.sa.SensorService.persistence;

import be.kdg.sa.SensorService.model.SensorMessage;
import be.kdg.sa.SensorService.model.SensorType;
import org.springframework.data.jpa.repository.JpaRepository;
import java.time.LocalDateTime;
import java.util.List;

public interface SensorMessageRepository extends JpaRepository<SensorMessage, Long> {

    List<SensorMessage> findAllBySensorTypeAndTimestampIsBetween(SensorType sensorType, LocalDateTime start, LocalDateTime end);

    List<SensorMessage> findAllBySensorType(SensorType sensorType);

}
