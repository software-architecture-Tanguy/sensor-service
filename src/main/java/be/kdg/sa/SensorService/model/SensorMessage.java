package be.kdg.sa.SensorService.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;
import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class SensorMessage implements Serializable {
    @Id
    @GeneratedValue
    private long id;
    @JsonProperty("timestamp")
    private LocalDateTime timestamp;
    @JsonProperty("xcoord")
    private double xCoord;
    @JsonProperty("ycoord")
    private double yCoord;
    @JsonProperty("sensorType")
    private SensorType sensorType;
    @JsonProperty("value")
    private double value;
}
