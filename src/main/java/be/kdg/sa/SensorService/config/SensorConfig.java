package be.kdg.sa.SensorService.config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SensorConfig {
    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }
}
