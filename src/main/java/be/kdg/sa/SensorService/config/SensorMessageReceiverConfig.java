package be.kdg.sa.SensorService.config;

import be.kdg.sa.SensorService.mapping.JSONService;
import be.kdg.sa.SensorService.messaging.SensorMessageReceiver;
import be.kdg.sa.SensorService.processing.Processor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.retry.backoff.FixedBackOffPolicy;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;

@Configuration
public class SensorMessageReceiverConfig {
    private final Processor processor;
    private final JSONService jsonService;

    @Value("${maxattemts}")
    private int maxAttemts;
    @Value("${backoffpolicy}")
    private long backOffPolicy;

    public SensorMessageReceiverConfig(Processor processor, JSONService jsonService) {
        this.processor = processor;
        this.jsonService = jsonService;
    }

    @Bean
    public RetryTemplate retryTemplate() {
        SimpleRetryPolicy retryPolicy = new SimpleRetryPolicy();
        retryPolicy.setMaxAttempts(maxAttemts);

        FixedBackOffPolicy backOffPolicy = new FixedBackOffPolicy();
        backOffPolicy.setBackOffPeriod(1000/*backOffPolicy*/);//TODO won't accept long value . . .

        RetryTemplate retryTemplate = new RetryTemplate();
        retryTemplate.setRetryPolicy(retryPolicy);
        retryTemplate.setBackOffPolicy(backOffPolicy);
        return retryTemplate;
    }

    @Bean
    public SensorMessageReceiver cameraMessageReceiver() {
        return new SensorMessageReceiver(processor, retryTemplate(), jsonService);
    }

}
