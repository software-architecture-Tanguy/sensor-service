package be.kdg.sa.SensorService.services;

import be.kdg.sa.SensorService.model.SensorMessage;
import be.kdg.sa.SensorService.model.SensorType;
import be.kdg.sa.SensorService.persistence.SensorMessageService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SensorServiceTest {
    @Autowired
    private SensorMessageService sensorMessageService;

    @Test
    public void testFilterTimeStampAndType() {
        LocalDateTime end = LocalDateTime.now();
        LocalDateTime start = end.minusDays(7);
        List<SensorMessage> sensormessages = sensorMessageService.filterByTypeAndTimestamp(SensorType.FINE_DUST, start, end);
        for (SensorMessage sensorMessage : sensormessages) {
            System.out.println(sensorMessage.toString());
        }

        Assert.assertNotNull(sensormessages);
    }
}
